user=$1
pass=$2
# Si no se descomentan las baseurl del centos repo fallara la instalacion de openshift hacia el final
sed -i 's/#baseurl=/baseurl=/g' /etc/yum.repos.d/CentOS-Base.repo
cp openshift-inventory ~/openshift-ansible/openshift-inventory
cd ~/openshift-ansible
git checkout release-3.6
git pull origin release-3.6
ansible-playbook -i openshift-inventory ~/openshift-ansible/playbooks/byo/config.yml  --user=$user --extra-vars "ansible_sudo_pass="$pass" openshift_disable_check=memory_availability,docker_storage,docker_image_availability"