yum install -y bind bind-utils
cp named.conf /etc/named.conf
mkdir /etc/named
mkdir /etc/named/zones
cp named/named.conf.local /etc/named/named.conf.local
cp named/zones/db.1.168.192 /etc/named/zones/db.1.168.192
cp named/zones/db.jrovira.org /etc/named/zones/db.jrovira.org
named-checkconf
systemctl start named
systemctl enable named
systemctl restart named
firewall-cmd --permanent --add-port=53/tcp
firewall-cmd --permanent --add-port=53/udp
firewall-cmd --reload

