$TTL    604800
@       IN      SOA     dns.jrovira.org. admin.jrovira.org. (
                  4       ; Serial
             604800     ; Refresh
              86400     ; Retry
            2419200     ; Expire
             604800 )   ; Negative Cache TTL
;
; name servers - NS records
     IN      NS      dns.jrovira.org.

; name servers - A records
dns.jrovira.org.          IN      A       192.168.1.10

; 192.168.1.0/24 - A records
dns.jrovira.org.            IN      A      192.168.1.10
nfs.jrovira.org.            IN      A      192.168.1.20
gluster1.jrovira.org.       IN      A      192.168.1.21
gluster2.jrovira.org.       IN      A      192.168.1.22
router.jrovira.org.         IN      A      192.168.1.30
master.jrovira.org.         IN      A      192.168.1.50
infra.jrovira.org.          IN      A      192.168.1.100
infra2.jrovira.org.         IN      A      192.168.1.101
app.jrovira.org.            IN      A      192.168.1.150
app2.jrovira.org.           IN      A      192.168.1.151

